const translate = {
    'auth/app-deleted': "L'application à été supprimée.",
    'auth/app-not-authorized': "L'application refuse la connection.",
    'auth/argument-error': "Les informations entrées ne conviennent pas.",
    'auth/invalid-api-key': "La clé d'api ne convient plus.",
    'auth/invalid-user-token': "L'adresse et le mot de passe fournit n'appartiennent à aucun compte.",
    'auth/network-request-failed': "Vous n'êtes pas connecté, nous ne parvenons pas a joindre le serveur.",
    'auth/operation-not-allowed': "Opération non permise.",
    'auth/requires-recent-login': "Votre session n'est plus securisée, vous devez vous reconnecter.",
    'auth/too-many-requests': "Le serveur reçoit trop de requêtes, veuillez ressayer dans quelques instants.",
    'auth/unauthorized-domain': "Le domaine n'est pas authorisé.",
    'auth/user-disabled': "Votre compte a été bloqué.",
    'auth/user-token-expired': "Votre session a expirée",
    'auth/web-storage-unsupported': "Votre navigateur ne supporte pas la gestion de données.",
    'auth/invalid-email': "Adresse e-mail non valide.",
    'auth/invalid-password': "Mot de passe non valide",
    'auth/email-already-exists': "L'adresse e-mail est déjà prise par un autre utilisateur.",
    'auth/user-not-found': "Aucun utilisateur ne correspond aux données fournit.",
    'auth/wrong-password': "Mauvais mot de passe, ou l'utilisateur n'a pas de mot de passe."
}
export default translate;