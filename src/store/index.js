import Vue from 'vue'
import Vuex from 'vuex'
import moduleUser from './userModule'
import moduleGlobal from './globalModule'
import moduleEvenements from './evenementsModule'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        currentUser: moduleUser,
        global: moduleGlobal,
        evenements: moduleEvenements
    }
})

export default store
