/*
States
    books       : []
Mutations
    addBook(books)   -> add book
    setBooks(books)   -> set books
Actions
    addBook(books)   -> async addBook
    setBooks(books)   -> async setBooks
Getter
    getBooks          -> return books
*/

const moduleBooks = {
    state: {
        books: []
    },
    mutations: {
        addBook(state, book) {
            state.books.push(book)
        },
        setBooks(state, books) {
          state.books = books
        }
    },
    actions: {
        addBook(context, book) {
            context.commit('addBook', book)
        },
        setBooks(context, books) {
            context.commit('setBooks', books)
        }
    },
    getters: {
        getBooks: state => {
            return state.books
        },
        getBookById: (state, id) => {
          return state.books.filter(b => b.id == id)
        }
    }
}

export default moduleBooks
