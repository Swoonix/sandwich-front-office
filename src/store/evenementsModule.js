/*
States
    evenements       : [] list of events
Mutations
    addEvenement(evenement)      -> add evenement
    setEvenements(evenements)    -> set evenements
Actions
    addEvenement(evenement)      -> async addEvenement
    setEvenements(evenements)    -> async setEvenements
Getter
    getEvents                    -> return evenements
    getEventById                 -> return evenement
*/

const moduleEvenements = {
    state: {
        evenements: []
    },
    mutations: {
        addEvenement(state, ev) {
            state.evenements.push(ev)
        },
        setEvenements(state, events) {
          state.evenements = events
        }
    },
    actions: {
        addEvenement(context, evenement) {
            context.commit('addEvenement', evenement)
        },
        setEvenements(context, evenements) {
            context.commit('setEvenements', evenements)
        }
    },
    getters: {
        getEvents: state => {
            return state.evenements
        },
        getEventById: state => id => {
          return state.evenements.filter(e => e.pk == id)[0]
        }
    }
}

export default moduleEvenements
