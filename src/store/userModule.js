/*
States
    isConnected     : boolean
    email           : string
    username        : string
    token           : string
    id              : int
Mutations
    log(user)   -> register user data in the store
    logout()    -> enleve l'etat connecté
Actions
    log(user)   -> async log
    logout()    -> async logout
Getter
    isConnected -> return true if user auth()
    currentUser -> return currentUser object
*/
import jwt_decode from 'jwt-decode';

const moduleUser = {
    state: {
        email: '',
        username: '',
        token: localStorage.getItem("sandwichToken"),
        isConnected: !!localStorage.getItem("sandwichToken"),
        id: null,
        points: 0
    },
    mutations: {
        log(state, user) {
            state.email = user.email;
            localStorage.setItem("sandwichToken", user.token);
            state.token = user.token;
            state.isConnected = true;
            state.id = jwt_decode(state.token).user_id;
        },
        logout(state) {
          state.email = '',
          state.username = '',
          state.token = null,
          state.isConnected = false,
          state.id = null,
          localStorage.removeItem('sandwichToken');
        },
        setToken(state) {
          state.token = localStorage.getItem("sandwichToken");
          if (state.token) {
            state.id = jwt_decode(state.token).user_id;
          }
        },
        setUser(state,user) {
          state.points = user.points;
          state.first_name = user.first_name;
          state.last_name = user.last_name;
          state.username = user.username;
          state.trajets = user.trajets;
          state.reservations = user.reservations;
          console.log(user);
        }
    },
    actions: {
        log(context, user) {
            context.commit('log', user)
        },
        logout(context) {
            context.commit('logout')
        },
        setToken(context, token) {
          context.commit('setToken')
        },
        setUser(context, user) {
          context.commit('setUser', user)
        }
    },
    getters: {
        isConnected: state => {
            return state.isConnected
        },
        currentUser: state => {
            return state
        },
        userId: state => {
            return state.id
        },
        getToken: state => {
          return state.token
        }
    }
}

export default moduleUser

/*
currentUser F L O W

  appStart -> get {localStorage} -> save in {store}
    if not null -> fetch user {serveur} -> save in {store}

  app get CurrentUser -> get currentUser {store}

  app Logout -> logout {Store} & {localStorage}

*/
