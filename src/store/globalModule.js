/*
States
    loading     : boolean
    error       : boolean
    success     : boolean
    message     : string
Mutations
    load(boolean)           -> change état loading
    enterError(error)       -> état error + message (error.message)
    enterSuccess(success)   -> état success + message (succes.message)
    dismiss                 -> enleve (etat error & etat sucess & message)
Actions
    load(boolean)           -> async load
    enterError(error)       -> async enter error
    enterSuccess(success)   -> async eterSuccess
    dismiss                 -> async dismiss
Getter
    isLoading()             -> return loading
    isError(error)          -> return error
    isSuccess(success)      -> return success
    getMessage()            -> return message
*/
import translate from '../assets/js/translateError'

const globalModule = {
    state: {
        loading: false,
        error: false,
        success: false,
        message: ''
    },
    mutations: {
        load(state, payload) {
            state.loading = payload
        },
        enterError(state, error) {
            state.error = true;
            state.message = (translate[error.code] ? translate[error.code] : error.message);
            state.loading = false;
        },
        enterSuccess(state, success) {
            state.success = true;
            state.message = success.message;
            state.loading = false;
        },
        dismiss(state) {
            state.error = false;
            state.success = false;
            state.message = '';
        }
    },
    actions: {
        load(context, payload) {
            context.commit('load', payload)
        },
        enterError(context, error) {
            context.commit('enterError', error)
        },
        enterSuccess(context, success) {
            context.commit('enterSuccess', success)
        },
        dismiss(context) {
            context.commit('dismiss')
        }
    },
    getters: {
        isLoading: state => {
            return state.loading
        },
        isError: state => {
            return state.isError
        },
        isSuccess: state => {
            return state.isSuccess
        },
        getMessage: state => {
            return state.message
        }
    }
}

export default globalModule
