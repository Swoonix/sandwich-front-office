import Vue from 'vue'
import Router from 'vue-router'

/* Components */
import store from '../store/index'

import Index from '@/components/Index'
import Prevention from '@/components/prevention'
import Books from '@/components/books/books'
import Evenements from '@/components/evenements/evenements'
import Evenement from '@/components/evenements/evenement'
import Dashboard from '@/components/dashboard'
//import Nowhere from '@/components/404'


Vue.use(Router)

let router = new Router({
  routes: [{
      path: '*',
      name: 'index',
      component: Index
    },
    {
      path: '/prevention',
      name: 'prevention',
      component: Prevention
    },
    {
      path: '/evenements',
      name: 'evenements',
      component: Evenements
    },
    {
      path: '/evenement/:id',
      name: 'evenement',
      component: Evenement
    },{
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        Auth: true
      }
    }/*,
    {
      path: '/404',
      name: '404',
      component: Nowhere
    }*/
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.Auth && !store.getters.isConnected) {
    // l'utilisateur n'est pas connecté tandis que la route le requiert
    // on le redirige vers le path de connection
    store.dispatch("enterError", {message: "Accés refusé, veuillez vous connecté."})
    next({
      path: '/'
    })
  } else {
    next()
  }
})

export default router;
