// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import store from './store/index'
import App from './App'
import router from './router'
import Http from '../config/http-common.js'
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.config.productionTip = false

// Vue use
Vue.use(Vuetify, {
  theme: {
    primary: '#388E3C',
    secondary: '#1E1E3E',
    accent: '#8c9eff',
    error: '#b71c1c',
    dark: '#1E1E2E'
  }
});
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBTtW26n2oh61pDhT4SJpEGEeu9rfAG2xE',
    libraries: 'places',
  }
})

// On s'assure que firebase est init pour continuer
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
});
