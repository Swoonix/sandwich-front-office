
/*
Les deux méthodes renvoient des promesses pour consumer l'api

  post(request, data)   -> renvoie une requete post sur request contenant data
  post(String, Object)  -> Promise

  get(request)          -> renvoie une requete get sur request
  get(string)           -> Promise
*/

import axios from 'axios';
import store from '../src/store/index'
//const api_url = "http://192.168.43.132:8080/rest_api/"
const api_url = "http://vps489622.ovh.net:8000/sandwich/rest_api/"


export default {
  post: function(request,data){
    if(store.getters.getToken) {
      return axios( api_url + request, {
        method: "post",
        data: JSON.stringify(data),
        headers:  { 'Authorization': 'JWT ' + store.getters.getToken,
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    } else {
      return axios( api_url + request, {
        method: "post",
        data: JSON.stringify(data),
        headers:  {
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    }
  },
  get: function(request) {
    if (store.getters.getToken) {
      return axios(api_url + request, {
        method: "get",
        headers: {
                  'Authorization': 'JWT ' + store.getters.getToken,
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    } else {
      return axios(api_url + request, {
        method: "get",
        headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    }
  },
  put: function(request) {
    if (store.getters.getToken) {
      return axios(api_url + request, {
        method: "put",
        headers: {
                  'Authorization': 'JWT ' + store.getters.getToken,
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    } else {
      return axios(api_url + request, {
        method: "get",
        headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/json"
                }
      })
    }
  }
}
