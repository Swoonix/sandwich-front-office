/*

Thomas FEL
17 / 10 / 2017 
Flappy Bird style for machine Learning
Games part

*/
let gameIsOn = false
let background = {
    width: 2040,
    phase: 0
}
let birds = []
let obstacles = []
let score = 0
let generation = 1
const params = {
    height: 700,
    width: 800,

    gravity: -0.9, /* -0.3 */
    jump: 10,
    speed: 6, /* 3 */

    number_of_obstacles: 3,
    number_of_birds: 5
}
let best_bird_biais = biais

function setNumber(number) {
    switch(number){
        case 1:     {params.number_of_birds = 1; break;}
        case 100:   {params.number_of_birds = 100; break;}
        case 200:   {params.number_of_birds = 200; break;}
    }    
}

function deepCopy(obj) {
    if (Object.prototype.toString.call(obj) === '[object Array]') {
        var out = [], i = 0, len = obj.length;
        for ( ; i < len; i++ ) {
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    if (typeof obj === 'object') {
        var out = {}, i;
        for ( i in obj ) {
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    return obj;
}


function makeBirds(n) {
    for(let i = 0; i < n; i ++){
        let b = new Bird()
        b.neural.biais = deepCopy(best_bird_biais)
        birds.push(b)
    }
}

function setup() {
    createCanvas(800, 700)
    frameRate(600)
    makeBirds(10)
    obstacles.push(new Obstacle())
    background.image = loadImage("assets/background.png")
    best_bird_biais = deepCopy(birds[0].neural.biais)

    gameIsOn = true
}

function draw() {
    if (gameIsOn) {
        drawBackground()
        askNeural()
        checkCollisions()        
        updateBirds()
        updateObstacles()
        updateInfo()
    } else {
        GenerateAndMutate()
    }
}

function drawBackground() {
    image(background.image,-background.phase,0, background.width, params.height, 0, 0, background.width, params.height)
    image(background.image,background.width-background.phase,0, background.width, params.height, 0, 0, background.width, params.height)
    if(background.phase < background.width) {
        background.phase ++
    } else {
        background.phase = 0
    }    
}

function updateBirds() {
    if(birds.length == 1){
        best_bird_biais = deepCopy(birds[0].neural.biais)
    }
    // update speed
    birds.forEach(b => {
            b.velocity += params.gravity
            b.y -= b.velocity
        })
        // filter if out screen
    birds = birds.filter(b => b.y >= -b.height && b.y <= params.height)
        // draw birds
    birds.forEach(b => {
        b.draw()
    })
    if (birds.length == 0) {
        gameIsOn = false
    }
}

function updateObstacles() {
    if (obstacles.length < params.number_of_obstacles) {
        if (obstacles[obstacles.length - 1].x < params.width - params.width / params.number_of_obstacles) {
            obstacles.push(new Obstacle)
        }
    }

    // update x position
    obstacles.forEach(o => {
            o.x -= params.speed
        })
        // filter if out screen
    obstacles = obstacles.filter(o => o.x > -o.largeur)
        // draw obstacles
    obstacles.forEach(o => {
        o.draw()
    })
}

function checkCollisions() {
    birds.forEach(bird => {
        obstacles.forEach(obstacle => {
            if ((bird.x + bird.width - bird.sprite.padding_width) < obstacle.x || (bird.x + bird.sprite.padding_width) > obstacle.x + obstacle.largeur) {} 
            else {
                if ( (bird.y + bird.sprite.padding_height ) < (obstacle.middle - obstacle.espacement / 2) || (bird.y + bird.height - bird.sprite.padding_height) > (obstacle.middle + obstacle.espacement / 2)) {
                    bird.isDead = true
                }
            }
        })
    })
    birds = birds.filter(b => !b.isDead)
}

function updateInfo() {
    score += 0.01 * params.speed
    document.getElementById("generation").innerText = "Generation: " + generation
    document.getElementById("score").innerText = "Score: " + Math.floor(score)
    document.getElementById("alive").innerText = "En vie: " + birds.length
}

class Bird {
    constructor() {
        this.velocity = 0
        this.x = params.width / 3
        this.y = params.height / 5        
        this.isDead = false
        this.img = loadImage("assets/dracolosse.png")
        this.sprite = {
            height: 100,
            width: 3740,
            nb: 44,
            phase_width: 85,
            phase: 0,
            padding_width: 15,
            padding_height: 10
        }
        this.scale = 0.75
        this.width = this.sprite.phase_width*this.scale - this.sprite.padding_width
        this.height = this.sprite.height*this.scale - this.sprite.padding_height
        this.fitness = 0
        this.neural = new NeuralNetwork()
    }
    flap() {
        this.velocity = params.jump
    }
    draw() {
        image(this.img,this.x,this.y,this.sprite.phase_width * this.scale,this.sprite.height * this.scale, Math.floor(this.sprite.phase) * this.sprite.phase_width,0,this.sprite.phase_width,this.sprite.height)
        if(this.sprite.phase < 43) {
            this.sprite.phase += 1
        } else {
            this.sprite.phase = 0
        }
    }
}

class Obstacle {
    constructor() {
        this.x = params.width
        this.espacement = Math.floor(Math.random() * (200 - 120) + 120)
        this.middle = Math.floor(Math.random() * (params.height - 160 - 160) + 160)
        this.largeur = 30
    }
    draw() {
        noStroke()
        fill(139,69,19)
        rect(this.x, 0, this.largeur, this.middle - this.espacement / 2)
        rect(this.x, this.middle + this.espacement / 2, this.largeur, params.height)
    }
}


function askNeural(){
    let first_obstacle = obstacles.filter(o => o.x > birds[0].x)[0]
    birds.forEach(b => {
        if(b.neural.activate(first_obstacle.x - b.x, first_obstacle.middle - b.y, b.velocity)) {
            b.flap()
        }
    })
}

function GenerateAndMutate() {

    makeBirds(params.number_of_birds)
    
    for(let i=0 ; i< birds.length ; i++) {
        birds[i].neural.mutate()
    }

    let old_bird = new Bird()
    old_bird.neural.biais = deepCopy(best_bird_biais)
    birds.push(old_bird)

    obstacles = []
    obstacles.push(new Obstacle())
    generation ++
    score = 0

    gameIsOn = true
}