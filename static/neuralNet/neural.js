function sigmoid(t) {
    return 1/(1+Math.pow(Math.E, -t));
}

class NeuralNetwork {
    constructor(){
        this.biais = biais
        this.init()
    }
    init(){
        this.neurons = [0.0,0.0,0.0,0.0,0.0,0.0]
        this.output = 0.0
    }
    activate(x,y,v){
        this.init()
        this.outputLayout(this.hiddenLayout(x,y,v))
        return sigmoid(this.output) > 0.5
    }
    hiddenLayout(x,y,v){
        for(let i = 0; i < this.neurons.length; i++){
            this.neurons[i] = 
            x * this.biais.hiddenLayer[i].bx + 
            v * this.biais.hiddenLayer[i].bv + 
            y * this.biais.hiddenLayer[i].by + 
            this.biais.hiddenLayer[i].cx + 
            this.biais.hiddenLayer[i].cy + 
            this.biais.hiddenLayer[i].cv
        }
        return this.neurons
    }
    outputLayout(neurons){
        neurons.forEach((neuron, index) => this.output += ( neuron * this.biais.outputLayer[index].b + this.biais.outputLayer[index].c ) )
    }
    mutate(){
        this.biais.hiddenLayer.forEach(b => {
            b.bx += ( (Math.random()* 2) -1 )
            b.by += ( (Math.random()* 2) -1 )
            b.bv += ( (Math.random()* 2) -1 )
            b.cx += ( (Math.random()* 2) -1 )
            b.cy += ( (Math.random()* 2) -1 )
            b.cv += ( (Math.random()* 2) -1 )
        })
        this.biais.outputLayer.forEach(b => {
            b.b += ( (Math.random()* 2) -1 )
            b.c += ( (Math.random()* 2) -1 )
        })
    }
}
